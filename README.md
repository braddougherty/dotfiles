# Brad’s dotfiles

After cloning the repo (doesn't matter where), run `./install` to set everything up.

Installation powered by [Dotbot](https://github.com/anishathalye/dotbot).

## Local Customizations

There are a number of hooks to make local customizations:

* `git`: `~/.gitconfig.local`
* `zsh` and `bash`: `~/.shell.local.before` / `~/.shell.local.after` to run before or after the main profile
* `bash` only: `~/.bashrc.local.before` / `~/.bashrc.local.after` to run before or after the main bash profile
* `zsh` only: `~/.zshrc.local.before` / `~/.zshrc.local.after` to run before or after the main zsh profile

## License

MIT © [Brad Dougherty](https://brad.is)
